angular.module('app', ['ionic'])

.run(function($ionicPlatform, User) {
  $ionicPlatform.ready(function() {
    User.init();
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'LoginController'
    })

    .state('app', {
      url: '/app',
      abstract: true,
      templateUrl: 'templates/menu.html',
      controller: 'AppCtrl'
  })

  .state('app.dash', {
    url: '/dash',
    views: {
      'menuContent': {
        templateUrl: 'templates/dash.html',
        controller: 'DashController'
      }
    }
  })

  .state('app.polls', {
      url: '/polls',
      views: {
        'menuContent': {
          templateUrl: 'templates/polls.html',
          controller: 'PollsController'
        }
      }
    })

    .state('app.profile', {
      url: '/profile',
      views: {
        'menuContent': {
          templateUrl: 'templates/profile.html',
          controller: 'ProfileController'
        }
      }
    })

  .state('mypoll', {
    url: '/questions/:id',
    templateUrl: 'templates/my-poll.html',
    controller: 'MyPollController'
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');
});
